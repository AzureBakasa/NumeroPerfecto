package cl.ubb.numerosperfectos;

public class NumerosPerfectos {

	public boolean CompruebaNumeroPerfecto(int numero) {
        int sumas=0;
        for(int i=1; i<numero-1; i++){
        	if(numero%i==0) //Comprueba si "i" es divisor del numero
        		sumas+=i; //suma los divisores
        }
	    if(sumas==numero) //compara si la suma de divisores es igual al numero ingresado
        	return true;
        else 
            return false;
	}      

}
