package cl.ubb.numerosperfectos;

import static org.junit.Assert.*;

import org.junit.Test;

public class NumerosPerfectosTest {

	@Test
	public void Ingresa28DevuelveTrue() {
		//arrange
		NumerosPerfectos numpe = new NumerosPerfectos();
		boolean resultado;
		//act
		resultado=numpe.CompruebaNumeroPerfecto(28);
		//assert
		assertEquals(resultado,true);
	}
	@Test
	public void Ingresa6DevuelveTrue() {
		//arrange
		NumerosPerfectos numpe = new NumerosPerfectos();
		boolean resultado;
		//act
		resultado=numpe.CompruebaNumeroPerfecto(6);
		//assert
		assertEquals(resultado,true);
	}
	@Test
	public void Ingresa496DevuelveTrue() {
		//arrange
		NumerosPerfectos numpe = new NumerosPerfectos();
		boolean resultado;
		//act
		resultado=numpe.CompruebaNumeroPerfecto(496);
		//assert
		assertEquals(resultado,true);
	}
	@Test
	public void Ingresa500DevuelveFalse() {
		//arrange
		NumerosPerfectos numpe = new NumerosPerfectos();
		boolean resultado;
		//act
		resultado=numpe.CompruebaNumeroPerfecto(500);
		//assert
		assertEquals(resultado,false);
	}
}
